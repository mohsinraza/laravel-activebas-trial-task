<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
</head>

<body>

    <div class="container">

        <div class="row my-3 py-3"></div>

        <h3 class="text-center">Events / Employees</h3>

        <div class="row">
            <div class="col-8 mx-auto">
                @if(!empty($errors->first()))
                    <div class="alert alert-danger">{{$errors->first()}}</div>
                @endif
                @if(!empty(session()->get('success')))
                    <div class="alert alert-success">{{session()->get('success')}}</div>
                @endif
            </div>
            <div class="col-8 mx-auto">

                <form method="POST" action="{{route('store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="file">File</label>
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input" id="file" required>
                                <label class="custom-file-label" for="file">Choose file...</label>
                            </div>
                            @error('file')
                                <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-4 pt-4">
                            <button type="submit" class="btn btn-primary mt-2">Import</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-12 mx-auto">

                <form method="GET" action="{{route('index')}}" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <div class="form-group">
                                <label for="events">Events</label>
                                <select data-live-search="true" title="Events" class="form-control" id="events" name="event">
                                    @if($events->count() > 0)
                                        @foreach ($events as $event)
                                            <option value="{{$event->id}}" @if(request()->event == $event->id) selected @endif>{{$event->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="form-group">
                                <label for="employees">Employees</label>
                                <select data-live-search="true" title="Employees" class="form-control" id="employees" name="employee">
                                    @if($employees->count() > 0)
                                        @foreach ($employees as $employee)
                                            <option value="{{$employee->id}}" @if(request()->employee == $employee->id) selected @endif>{{$employee->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input type="date" class="form-control" id="date" name="date" value="{{request()->date ?? ''}}">
                            </div>
                        </div>
                        <div class="form-group col-md-2 pt-4">
                            <button type="submit" class="btn btn-primary mt-2">Filter</button>
                            <a href="{{route('index')}}" class="btn btn-default mt-2">Reset</a>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Participation ID</th>
                            <th>Employee Name</th>
                            <th>Employee Email</th>
                            <th>Event Name</th>
                            <th>Event Date</th>
                            <th>Participation Fee</th>
                            <th>Version</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($participations->count() > 0)
                            @foreach ($participations as $participation)
                                <tr>
                                    <th scope="row">{{$participation->id}}</th>
                                    <th>{{$participation->employees->name}}</th>
                                    <th>{{$participation->employees->email}}</th>
                                    <th>{{$participation->events->name}}</th>
                                    <th>{{$participation->events->date}}</th>
                                    <th>{{$participation->fee}}</th>
                                    <th>{{$participation->version ?? '-'}}</th>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5"></td>
                            <td>Total: {{$participations->sum('fee')}}</td>
                            <td colspan="3"></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
    <script>
        $(function () {
            $('#file').on('change',function(){
                var fileName = $(this).val();
                $(this).next('.custom-file-label').html(fileName);
            })
            $('select').selectpicker();
        });
    </script>
</body>

</html>
