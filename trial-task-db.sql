-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 02, 2021 at 12:16 AM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testing`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Reto Fanzen', 'reto.fanzen@no-reply.rexx-systems.com', '2021-04-01 13:51:26', '2021-04-01 13:51:26'),
(2, 'Leandro Bußmann', 'leandro.bussmann@no-reply.rexx-systems.com', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(3, 'Hans Schäfer', 'hans.schaefer@no-reply.rexx-systems.com', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(4, 'Mia Wyss', 'mia.wyss@no-reply.rexx-systems.com', '2021-04-01 13:51:38', '2021-04-01 13:51:38');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `date`, `created_at`, `updated_at`) VALUES
(1, 'PHP 7 crash course', '2019-09-04', '2021-04-01 13:49:59', '2021-04-01 13:49:59'),
(2, 'International PHP Conference', '2019-10-21', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(3, 'code.talks', '2019-10-24', '2021-04-01 13:51:38', '2021-04-01 13:51:38');

-- --------------------------------------------------------

--
-- Table structure for table `participations`
--

CREATE TABLE `participations` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `fee` decimal(10,2) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participations`
--

INSERT INTO `participations` (`id`, `employee_id`, `event_id`, `fee`, `version`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0.00', '', '2021-04-01 13:51:37', '2021-04-01 13:51:37'),
(2, 1, 2, '1485.99', '', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(3, 2, 2, '657.50', '', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(4, 3, 1, '0.00', '', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(5, 4, 1, '0.00', '', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(6, 4, 2, '657.50', '1.1.3', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(7, 1, 3, '474.81', '', '2021-04-01 13:51:38', '2021-04-01 13:51:38'),
(8, 3, 3, '534.31', '1.1.3', '2021-04-01 13:51:38', '2021-04-01 13:51:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participations`
--
ALTER TABLE `participations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `participations`
--
ALTER TABLE `participations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
