<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $cast = ['date' => 'Date'];
    protected $fillable = ['id', 'name', 'date'];

    /**
     * Get the user that owns the Event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function participation()
    {
        return $this->belongsTo(Participation::class, 'event_id', 'id');
    }
}
