<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['id', 'name', 'email'];
    /**
     * Get the user that owns the Event
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function participation()
    {
        return $this->belongsTo(Participation::class, 'employee_id', 'id');
    }
}
