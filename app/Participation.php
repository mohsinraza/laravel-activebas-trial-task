<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participation extends Model
{
    protected $fillable = ['id', 'employee_id', 'event_id', 'fee', 'version'];

   /**
    * Get all of the events for the Participation
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
   public function events()
   {
       return $this->hasOne(Event::class, 'id', 'event_id');
   }

   /**
    * Get all of the employee for the Participation
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
   public function employees()
   {
       return $this->hasOne(Employee::class, 'id', 'employee_id');
   }

}
