<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Event;
use App\Participation;

class HomeController extends Controller
{

    public function index(Request $request){

        $participations = Participation::orderByDesc('id');

        if(!empty($request->event))
        {
            $participations = $participations->where('event_id', $request->event);
        }
        if(!empty($request->employee))
        {
            $participations = $participations->where('employee_id', $request->employee);
        }

        $participations = $participations->whereHas('events', $filter = function($query) use ($request){
            if(!empty($request->date))
            {
                return $query->where('date', $request->date);
            }
        });

        $participations = $participations->with(['events' => $filter]);
        $participations = $participations->with('employees');

        $participations = $participations->get();

        return view('index')
            ->with('events', Event::all())
            ->with('employees', Employee::all())
            ->with('participations', $participations);
    }

    public function store(Request $request){

        $validated = $request->validate([
            'file' => 'required|file',
        ]);

        $fileName = $validated['file']->getClientOriginalName();

        $validated['file']->move(storage_path('uploads'), $fileName);

        $contents = file_get_contents(storage_path('uploads/'. $fileName));
        $contents = json_decode($contents, true);

        if(
            count($contents) == 0 ||
            empty($contents[0]['employee_name']) ||
            empty($contents[0]['employee_mail']) ||
            empty($contents[0]['event_id']) ||
            empty($contents[0]['event_name'])
        ){

            return back()->withErrors('Invalid File Format.');
        }


        foreach($contents as $content)
        {

            $participation = Participation::find($content['participation_id']);
            if(!empty($participation))
            {
                continue;
            }

            $event = Event::find($content['event_id']);
            if(empty($event))
            {
                $event = new Event();
                $event->id = $content['event_id'];
                $event->name = $content['event_name'];
                $event->date = $content['event_date'];
                $event->save();
            }

            $employee = Employee::where('email' , $content['employee_mail'])->first();
            if(empty($employee))
            {
                $employee = new Employee();
                $employee->id = NULL;
                $employee->name = $content['employee_name'];
                $employee->email = $content['employee_mail'];
                $employee->save();
            }

            $participation = new Participation();
            $participation->id = $content['participation_id'];
            $participation->employee_id = $employee->id;
            $participation->event_id = $event->id;
            $participation->fee = $content['participation_fee'];
            $participation->version = $content['version'] ?? '';

            $participation->save();
        }

        return back()->withSuccess('File Upload Successfully.');

    }

}
